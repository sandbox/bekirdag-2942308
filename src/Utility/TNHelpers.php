<?php 

namespace Drupal\tn_google_places\Utility;

use Drupal\tn_google_places\Controller\TNGPController;
use Drupal\taxonomy\Entity\Term;

class TNHelpers { 
  /**
   * {@inheritdoc}
   */
  public function placeIDtotermID($place_id) {
    $connection = \Drupal::database();
    $term_id = $connection->query("SELECT entity_id FROM {taxonomy_term__field_place_id} WHERE field_place_id_value = :place_id", [':place_id' => $place_id])->fetchField();
    
    if(!$term_id) {
      $term_id = $this->placeToNewTerm($place_id);
    }
    
    return $term_id;
  }
  
  public function placeToNewTerm($place_id) {
    $place_data = new TNGPController;
    $place_object = $place_data->placeDetails($place_id);
    
    $lat = $place_object->result->geometry->location->lat;
    $long = $place_object->result->geometry->location->lng;
    $photo_reference = $place_object->result->photos[0]->photo_reference;
//     $google_image = $place_data->placePhoto($photo_reference);

    $places = $place_object->result->address_components;
    
    krsort($places);
    
    $parent = 0;
    $names = array();
    foreach ($places as $place) {
      if(!in_array($place->long_name,$names)) {
        $parent = $this->placeObjectToSingleTerm($place,$parent);
        $names[] = $place->long_name;
      }
    }
    
    $term = Term::load($parent);
    $term->field_google_image->setValue($photo_reference);
    $term->field_latitude->setValue($lat);
    $term->field_longitude->setValue($long);
    $term->field_place_id->setValue($place_id);
    $term->Save();
    
    // This will return the last added term id
    return $parent;
  }
  
  public function placeObjectToSingleTerm($place_object,$parent) {
    // Check if the term exist
    $connection = \Drupal::database();
    $term_id = $connection->query("SELECT ttfd.tid FROM {taxonomy_term_field_data} ttfd inner join {taxonomy_term_hierarchy} as tth on tth.tid = ttfd.tid WHERE ttfd.name = :name and tth.parent = :parent", [':name' => $place_object->long_name, ':parent' => $parent])->fetchField();
    if($term_id) {
      return $term_id;
    }
    
    // Add new term if doesn't exist
    // Create the taxonomy term.
    $new_term = Term::create([
        'name' => $place_object->long_name,
        'vid' => 'location',
        'parent' => array($parent),
        ]);
    // Save the taxonomy term.
    $new_term->save();
    // Return the taxonomy term id.
    return $new_term->id();
  }
}