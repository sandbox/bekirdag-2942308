<?php
/**
 * @file
 * Contains \Drupal\tn_google_places\Controller\TNGPController.
 */
namespace Drupal\tn_google_places\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Unicode;

// https://maps.googleapis.com/maps/api/place/autocomplete/json?input=kad%C4%B1k%C3%B6&types=(regions)&language=en_EN&key=xxxxxxxxx

class TNGPController extends ControllerBase {
  
  public function getAPIKey() {
    $config = \Drupal::config('tn_google_places.admin_settings');
    $this->api_key = $config->get('tngp_api_key');
    return $this->api_key;
  }
  
  public function searchPlace($name) {
    return new JsonResponse($this->searchPlaceArray($name));
  }
  
  private function searchPlaceArray($name) {
    $client = \Drupal::httpClient();
  
    $request = $client->get('https://maps.googleapis.com/maps/api/place/autocomplete/json',
        [
        'query' => [
          'input' => $name,
          'types' => "(regions)",
          'language' => "en_EN",
          'key' => $this->getAPIKey(),
          ]
        ]
    );
    $response = json_decode($request->getBody());
    return $response;
  }
  
  public function placeDetails($placeID) {
    $client = \Drupal::httpClient();
  
    $request = $client->get('https://maps.googleapis.com/maps/api/place/details/json',
        [
          'query' => [
            'placeid' => $placeID,
            'key' => $this->getAPIKey(),
          ]
        ]
    );
    $response = json_decode($request->getBody());
    return $response;
  }
  
  public function placePhoto($photo_reference) {
    $client = \Drupal::httpClient();
  
    $request = $client->get('https://maps.googleapis.com/maps/api/place/photo',
        [
          'query' => [
            'maxwidth' => "1440",
            'photoreference' => $photo_reference,
            'key' => $this->getAPIKey(),
          ]
        ]
    );
    $response = json_decode($request->getBody());
    return $response;
  }
  
  public function autocomplete(request $request) {
    $matches = array();
    $string = $request->query->get('q');
    
    if ($string) {
      $matches = array();
      $predictions = $this->searchPlaceArray($string);
      foreach ($predictions->predictions as $item) {
      	$matches[] = ['value' => $item->description, 'label' => $item->description, 'hidden' => $item->place_id ];
      }
    }
    return new JsonResponse($matches);
  }
  
}