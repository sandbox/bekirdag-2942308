<?php

namespace Drupal\tn_google_places\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'tn_google_places_autocomplete' widget.
 *
 * @FieldWidget(
 *   id = "tn_google_places_autocomplete",
 *   module = "tn_google_places",
 *   label = @Translation("TN Google Places Autocomplete"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class TNGPAutocompleteWidget extends WidgetBase{

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = isset($items[$delta]->value) ? $items[$delta]->value : '';
    $element += [
      '#type' => 'textfield',
      '#default_value' => $value,
      '#autocomplete_route_name' => 'tn_google_places.autocomplete',
      '#size' => $this->getSetting('size'),
      '#prefix' => '<div id="tn_google_place_api_wrapper">',
      '#suffix' => '</div>',
      '#attached' => [
        'library' => [
          'tn_google_places/place_id_work',
        ],
      ],
    ];
    
    return ['value' => $element];
  }
  
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
    // Create a default setting 'size', and
    // assign a default value of 500
    'size' => 500,
    ] + parent::defaultSettings();
  }
  
  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['size'] = [
    '#type' => 'number',
    '#title' => t('Size of textfield'),
    '#default_value' => $this->getSetting('size'),
    '#required' => TRUE,
    '#min' => 1,
    ];
  
    return $element;
  }
  
  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
  
    $summary[] = t('Textfield size: @size', array('@size' => $this->getSetting('size')));
  
    return $summary;
  }
}
