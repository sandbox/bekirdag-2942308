<?php

namespace Drupal\tn_google_places\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'tn_google_places_rgb' field type.
 *
 * @FieldType(
 *   id = "tn_google_places_field",
 *   label = @Translation("TNGP field type"),
 *   module = "tn_google_places",
 *   description = @Translation("TNGP field type."),
 *   default_widget = "tn_google_places_autocomplete",
 *   default_formatter = "tn_google_places_autocomplete_formatter"
 * )
 */
class TNGPField extends FieldItemBase {
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('String value'));

    return $properties;
  }

}
