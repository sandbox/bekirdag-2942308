<?php

namespace Drupal\tn_google_places\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'tn_google_places_simple_text' formatter.
 *
 * @FieldFormatter(
 *   id = "tn_google_places_autocomplete_formatter",
 *   module = "tn_google_places",
 *   label = @Translation("TNGP Field formatter"),
 *   field_types = {
 *     "tn_google_places_field"
 *   }
 * )
 */
class TNGPFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'string',
        '#value' => $item->value,
      ];
    }

    return $elements;
  }

}
