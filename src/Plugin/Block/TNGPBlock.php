<?php

namespace Drupal\tn_google_places\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides TNGP form block
 *
 * @Block(
 *   id = "tngp_form_block",
 *   admin_label = @Translation("TNGP Form block"),
 *   category = @Translation("TNGP blocks"),
 * )
 */
class TNGPBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\tn_google_places\Form\TNGPAutocomplete');
    return $form;
  }
  
}