<?php

namespace Drupal\tn_google_places\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for Module Filter.
 */
class TNGooglePlacesSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tn_google_places_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('tn_google_places.admin_settings');
    $form = parent::buildForm($form, $form_state);

    $form['modules'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('TN Google Places API settings'),
      '#description' => $this->t('TN Google Places API settings page'),
      '#collapsible' => FALSE,
    ];
    $form['modules']['tngp_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Places API KEY'),
      '#description' => $this->t('To use this module, please enter your Places API KEY.'),
      '#default_value' => $config->get('tngp_api_key'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('tn_google_places.admin_settings')
      ->set('tngp_api_key', $values['tngp_api_key'])
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['tn_google_places.admin_settings'];
  }

}
