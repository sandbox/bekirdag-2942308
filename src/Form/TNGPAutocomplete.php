<?php

namespace Drupal\tn_google_places\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\tn_google_places\Utility\TNHelpers;
use Symfony\Component\HttpFoundation\RedirectResponse;

class TNGPAutocomplete extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tn_google_places_autocomplete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    
    $form['input_fields']['place'] = array(
        '#type' => 'textfield',
        '#autocomplete_route_name' => 'tn_google_places.autocomplete',
        '#prefix' => '<div id="tn_google_place_api_wrapper">',
        '#suffix' => '</div>',
        '#attached' => [
          'library' => [
            'tn_google_places/place_id_work',
           ],
         ],
    );
    $form['input_fields']['datepicker'] = array(
        '#type' => 'textfield',
        '#prefix' => '<div id="tn_google_datepicker_wrapper">',
        '#suffix' => '</div>',
    );
    $form['input_fields']['place_id'] = array(
        '#type' => 'hidden',
        '#attributes' => array(
            'id' => 'place_id'
        )
    );
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Search'),
    );
    
    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $element = $form['input_fields']['place'];
    $place = $form_state->getValue($element['#parents']);
    
    $element = $form['input_fields']['datepicker'];
    $daterange = $form_state->getValue($element['#parents']);
    
    $element = $form['input_fields']['place_id'];
    $place_id = $form_state->getValue($element['#parents']);
    
    $helper = new TNHelpers;
    $term_id = $helper->placeIDtotermID($place_id);
    
    if($term_id) {
//       return new RedirectResponse(\Drupal::url('user.page'));
    }
    

//     parent::submitForm($form, $form_state);
  }


}
