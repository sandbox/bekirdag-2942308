/**
 * @file
 *   Javascript for the TN Google Places API.
 */

(function ($, Drupal, window) {

  'use strict';
  var place_id = "";
  Drupal.behaviors.tngpapi = {
    attach: function (context, settings) {
    	var autocomplete = Drupal.autocomplete;
    	$("#tn_google_place_api_wrapper .form-autocomplete").autocomplete({
	      select: function( event, ui ) {
	    	  place_id = ui.item.hidden;
	    	  console.log(place_id);
	    	  drupalSettings.place_id = place_id;
	    	  $("#place_id").val(place_id);
	      }
	    });
    	$("#tn_google_datepicker_wrapper input").daterangepicker();
    }
  };

})(jQuery, Drupal, window);
